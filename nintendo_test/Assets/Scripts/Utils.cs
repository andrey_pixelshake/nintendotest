﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Utils
{
    public static Vector3 GetRandomDirection()
    {
        Vector2 dir = Random.insideUnitCircle.normalized;
        return new Vector3(dir.x, 0, dir.y);
    }

    public static Vector3 GetDirectionTo(this Transform transform, Vector3 point)
    {
        return new Vector3(point.x - transform.position.x, 0, point.z - transform.position.z).normalized;
    }

    public static T GetRandom<T>(this T[] array)
    {
        return array[Random.Range(0, array.Length)];
    }

    public static T GetRandom<T>(this IList<T> array)
    {
        return array[Random.Range(0, array.Count())];
    }
}
