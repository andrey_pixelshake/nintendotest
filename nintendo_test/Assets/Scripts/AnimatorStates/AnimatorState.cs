﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AnimatorState : StateMachineBehaviour
{
    [SerializeField]
    private EState _stateType;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        State[] statesAll = animator.GetComponents<State>();
        List<State> states = statesAll.Where(s => s.StateType == _stateType).ToList();
        if (states.Count() == 0) return;

        State state = states.GetRandom();
        state.enabled = true;
        state.Init(stateInfo);
        state.Enter();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        State[] states = animator.GetComponents<State>();
        State state = states.FirstOrDefault(s => s.enabled == true);
        if (state)
        {
            state.Exit();
            state.enabled = false;
        }
    }
}
