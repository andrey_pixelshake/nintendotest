﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PenguinIdleState : State
{
    [SerializeField]
    private Settings _settings;

    private Penguin _penguin;

    public override EState StateType { get { return EState.Idle; } }

    protected override void Awake()
    {
        base.Awake();
        _penguin = GetComponent<Penguin>();
        _penguin.TargetDirecton = Utils.GetRandomDirection();
    }

    public override void Enter()
    {
        base.Enter();

		_animator.SetBool("wall", false);
    }

    public override void Tick()
    {
        base.Tick();

		_rigidbody.velocity = _penguin.Directon.normalized * _settings.penguinIdleSpeed;
    }

    public override void Exit()
    {
        base.Exit();

		if(UnityEngine.Random.Range(0f, 1f) <= _settings.penguinGlideChance)
        {
			_animator.SetBool("glide", true);
		}
        else
		{
			_animator.SetBool("glide", false);
		}
    }
}
