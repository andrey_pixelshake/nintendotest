﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PenguinWaitState : State
{
    [SerializeField]
    private Settings _settings;

    private Penguin _penguin;

    public override EState StateType { get { return EState.Wait; } }

    protected override void Awake()
    {
        base.Awake();
        _penguin = GetComponent<Penguin>();
        _penguin.TargetDirecton = Utils.GetRandomDirection();
    }

    public override void Enter()
    {
        base.Enter();

        _rigidbody.velocity = Vector3.zero;
    }

    public override void Exit()
    {
        base.Exit();

        _penguin.TargetDirecton = Utils.GetRandomDirection();
        _penguin.Directon = Utils.GetRandomDirection();
        Quaternion q = Quaternion.LookRotation(_penguin.Directon);
    }
}
