﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PenguinGlideState : State
{
    [SerializeField]
    private Settings _settings;

    private Penguin _penguin;

    public override EState StateType { get { return EState.Glide; } }

    protected override void Awake()
    {
        base.Awake();
        _penguin = GetComponent<Penguin>();
    }

    public override void Enter()
    {
        base.Enter();
              
        _rigidbody.velocity = Vector3.zero;
        _penguin.Directon = _penguin.TargetDirecton;
    }

    public override void Tick()
    {
        base.Tick();

        _rigidbody.velocity = _penguin.Directon * _settings.penguinGlideSpeed;
    }
}
