﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PenguinRunState : State
{
    [SerializeField]
    private Settings _settings;

    private Penguin _penguin;
	private float _currentChangeRotationTime;
	private float _currentCanWaitTime;
    private float _angle;
	private bool _canWait;

    public override EState StateType { get { return EState.Run; } }

    protected override void Awake()
    {
        base.Awake();
        _penguin = GetComponent<Penguin>();
    }

    public override void Enter()
    {
		_animator.SetBool("can_wait", false);

        base.Enter();

        _rigidbody.velocity = Vector3.zero;
        _currentChangeRotationTime = GetRandomTime();

		_canWait = UnityEngine.Random.Range(0f, 1f) <= _settings.penguinWaitDuringRunChance;
		if(_canWait)
		{
			_currentCanWaitTime = UnityEngine.Random.Range(_settings.penguinTimeCanWaitMin, _settings.penguinTimeCanWaitMax);
		}
    }

	public override void Exit()
	{
		_animator.SetBool("can_wait", false);

		base.Exit();
	}

	private float GetRandomTime()
    {
        return UnityEngine.Random.Range(_settings.penguinTimeChangeDirMin, _settings.penguinTimeChangeDirMax);
    }

    public override void Tick()
    {
        base.Tick();

		if (_currentChangeRotationTime <= 0)
        {
            _currentChangeRotationTime = GetRandomTime();
			_angle = UnityEngine.Random.Range(-_settings.penguinRunRotationMaxAngle, _settings.penguinRunRotationMaxAngle);
        }

		if(_canWait)
		{
			if(_currentCanWaitTime <= 0)
			{
				_animator.SetBool("can_wait", true);
			}

			_currentCanWaitTime -= Time.deltaTime;
		}

		if(_penguin.IsNearWall || _penguin.IsAroundExit)
        {
            Quaternion qCenter = Quaternion.LookRotation(transform.GetDirectionTo(Vector3.zero));
            Quaternion qEscape = Quaternion.LookRotation(_penguin.EscapeDirecton);

            Quaternion lerpQ = Quaternion.Slerp(qEscape, qCenter, Time.deltaTime * _settings.penguinRotationSpeed);
			_penguin.TargetDirecton = lerpQ * Vector3.forward;
        }
        else
        {
			if(_penguin.IsAroundExit)
			{
				_penguin.TargetDirecton = _penguin.EscapeDirecton;
			}
			else
			{
				_penguin.TargetDirecton = Quaternion.AngleAxis(_angle, Vector3.up) * _penguin.EscapeDirecton;
			}
        }

		_rigidbody.velocity = _penguin.Directon.normalized * _settings.penguinRunSpeed;

        _currentChangeRotationTime -= Time.deltaTime;
    }
}
