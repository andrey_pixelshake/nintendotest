﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PenguinWallState : State
{
    private Penguin _penguin;

    public override EState StateType { get { return EState.Wall; } }

    protected override void Awake()
    {
        base.Awake();

        _penguin = GetComponent<Penguin>();

    }

    public override void Enter()
    {
        base.Enter();

		_animator.SetBool("wall", false);

        _rigidbody.velocity = Vector3.zero;

        Vector3 dir = transform.GetDirectionTo(Vector3.zero);
        _penguin.TargetDirecton = dir;

    }

	public override void Exit()
	{
		base.Exit();      
	}

	public override void Tick()
    {
        base.Tick();

        Vector3 dir = transform.GetDirectionTo(Vector3.zero);
        _penguin.TargetDirecton = dir;
    }
}
