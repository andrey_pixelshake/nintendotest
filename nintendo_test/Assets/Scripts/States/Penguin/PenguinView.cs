﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(StateHandler))]
public class PenguinView : MonoBehaviour
{
    [SerializeField]
    private GameObject _viewNornal;
    [SerializeField]
    private GameObject _viewGlide;

    private StateHandler _stateHandler;
    private void Awake()
    {
        _stateHandler = GetComponent<StateHandler>();
    }

    private void Start()
    {
        _stateHandler.StateAction += OnStateEnter;
    }

    private void OnDestroy()
    {
        _stateHandler.StateAction -= OnStateEnter;
    }

    private void OnStateEnter(EState state)
    {
        _viewNornal.SetActive(state != EState.Glide);
        _viewGlide.SetActive(state == EState.Glide);
    }
}
