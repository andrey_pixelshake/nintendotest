﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Penguin : MonoBehaviour
{
    public Settings _settings;
    public Vector3 Directon { get; set; }
    public Vector3 TargetDirecton { get; set; }
    public Vector3 EscapeDirecton { get; set; }
	public Vector3 WallNormal { get; set; }
    public bool IsNearWall { get; set; }
	public bool IsAroundExit { get; set; }

	public StateHandler StateHandler { get; private set; }

    private Animator _animator;
	private Rigidbody _rigidbody;
    private List<Collision> CollisionsTransform;
    private List<Collider> ColliderTransform;

	private float _timeChangeWall = 0.2f;
	private float _curTimeChangeWall = 0f;
	private bool _isNearWallMark = false;

    private void Awake()
    {
		_curTimeChangeWall = _timeChangeWall;
        Directon = new Vector3(0, 0, 1);
        //TargetDirecton = new Vector3(0, 0, 1);
        _animator = GetComponent<Animator>();
        StateHandler = GetComponent<StateHandler>();
		_rigidbody = GetComponent<Rigidbody>();
        CollisionsTransform = new List<Collision>();
        ColliderTransform = new List<Collider>();
    }
       
	private void OnCollisionStay(Collision c)
	{
		if (c.gameObject.tag == "wall")
        {         
            CollisionsTransform.Add(c);
        }
        
		if (c.gameObject.tag == "penguin")
        {
			Penguin p = c.gameObject.GetComponent<Penguin>();
			if(p.StateHandler.CurrentState == EState.Run)
			{
				EscapeToCenter(p.TargetDirecton);
			}

        }
	}

	private void OnTriggerStay(Collider c)
	{
		if (c.gameObject.tag == "player")
        {
			ColliderTransform.Add(c);
        }      
	}

	private void OnTriggerEnter(Collider c)
    {      
		if (c.gameObject.tag == "exit")
        {
            Destroy(gameObject);
        }

		if (c.gameObject.tag == "around_exit")
        {
            IsAroundExit = true;
        }
    }

	private void OnTriggerExit(Collider c)
	{
		if (c.gameObject.tag == "around_exit")
        {
            IsAroundExit = false;
        }
	}

	private void FixedUpdate()
	{   
		WallNormal = UpdateWallNormal();
		UpdateEscapeDirection();

		CollisionsTransform.Clear();
		ColliderTransform.Clear();

		float speedRotation = _settings.penguinRotationSpeed;
		if (StateHandler.CurrentState == EState.Run)
		{
			speedRotation = _settings.penguinRunRotationSpeed;
		}

		if (TargetDirecton != Vector3.zero)
		{
			Quaternion q = Quaternion.LookRotation(TargetDirecton);
			Quaternion lerpQ = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speedRotation);
			Directon = lerpQ * Vector3.forward;
			transform.rotation = lerpQ;
		}

		Debug.DrawRay(transform.position, Directon, Color.red);
		Debug.DrawRay(transform.position, TargetDirecton, Color.green);
		Debug.DrawRay(transform.position, WallNormal, Color.black);
		Debug.DrawRay(transform.position, EscapeDirecton, Color.magenta);
	}

    public void EscapeToCenter(Vector3 dir)
	{      
		if (ColliderTransform.Count > 0) return;
        
		float angle = UnityEngine.Random.Range(-_settings.penguinRunRotationMaxAngle, _settings.penguinRunRotationMaxAngle);
		Vector3 tempDir = (Quaternion.AngleAxis(angle, Vector3.up) * dir);
		tempDir.y = 0;
		EscapeDirecton = tempDir.normalized;

        _animator.SetBool("run", true);
	}

    private void UpdateEscapeDirection()
	{
		Vector3 sum = Vector3.zero;
        int count = 0;
        foreach (Collider c in ColliderTransform)
        {
            sum += c.transform.position - transform.position;
            count++;
        }

        if (count > 0)
        {
            Vector3 dir = (Vector3.zero - sum / count);

            dir.y = 0;
			EscapeDirecton = dir.normalized;

			_animator.SetBool("run", true);
        }
		else
		{
			_animator.SetBool("run", false);
		}
	}

	private Vector3 UpdateWallNormal()
	{
		Vector3 sum = Vector3.zero;
        int count = 0;
              
		//Debug.Log("CollisionsTransform count " + CollisionsTransform.Count);
                 
		foreach (Collision c in CollisionsTransform)
        {
            foreach (var p in c.contacts)
            {
				sum += p.normal;
                count++;
            }
        }

		if (count > 0)
        {
			if(!IsNearWall)
			{
				IsNearWall = true;
				_animator.SetBool("wall", true);
			}
   
            Vector3 dir = (sum / count);

			dir.y = 0;
			return dir;         
        }
		else
		{
			IsNearWall = false;
		}
        
		return Vector3.zero;
	}

}
