﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Npc : MonoBehaviour {

    [SerializeField]
    private Settings _settings;
	[SerializeField]
	private Transform _npcMovementPointsContaier;

    private Rigidbody _rigidbody;
    private Vector3 _targetDirection;
	private Animator _animator;

    private float _currentTime;

	private int _pointsCount;
	private int _currentPointIndex = 0;
	private List<Transform> _movementsPoints = new List<Transform>();

    void Awake () {
        _rigidbody = GetComponent<Rigidbody>();
        _currentTime = GetRandomTime();
        _targetDirection = Utils.GetRandomDirection();
		_animator = GetComponent<Animator>();
		_animator.SetBool("run", true);

		_movementsPoints = _npcMovementPointsContaier.Cast<Transform>().ToList();
		_pointsCount = _movementsPoints.Count;
    }
	
    private float GetRandomTime()
    {
		return Random.Range(_settings.npcChangeDirectionTimeMin, _settings.npcChangeDirectionTimeMax);
    }

    private void OnTriggerEnter(Collider c)
    {
		if (_pointsCount > 0) return;

        if (c.gameObject.tag == "around_exit")
        {         
            _targetDirection = transform.GetDirectionTo(Vector3.zero);
            _currentTime = GetRandomTime();
        }
    }

    void Update ()
    {
		if (_pointsCount > 0)
		{
			Transform nextPoint = _movementsPoints[_currentPointIndex];
			_targetDirection = transform.GetDirectionTo(nextPoint.position);
		}

        if(_currentTime <= 0)
        {
            _currentTime = GetRandomTime();

			if(_pointsCount > 0)
			{     
				_currentPointIndex++;

				if(_currentPointIndex == _pointsCount)
				{
					_currentPointIndex = 0;
				}

				Debug.Log("_currentPointIndex " + _currentPointIndex);
			}
			else
			{
				if (Random.Range(0, 2) == 0)
                {
                    _targetDirection = transform.GetDirectionTo(Vector3.zero);
					_targetDirection = (Quaternion.AngleAxis(Random.Range(-80, 80), Vector3.up) * _targetDirection);
                }
                else
                {
                    _targetDirection = Utils.GetRandomDirection();
                }
			}         
        }

        Quaternion q = Quaternion.LookRotation(_targetDirection);
        Quaternion lerpQ = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * _settings.npcRotationSpeed);
        Vector3 dir = lerpQ * Vector3.forward;

        _rigidbody.AddForce(dir * _settings.playerSpeed);

        if (_rigidbody.velocity.magnitude > _settings.playerSpeedMax)
        {
            _rigidbody.velocity = _rigidbody.velocity.normalized * _settings.playerSpeedMax;
        }

        transform.rotation = lerpQ;

        _currentTime -= Time.deltaTime;
    }
}
