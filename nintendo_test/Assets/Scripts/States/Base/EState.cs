﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum EState
{
    Wait,
    Idle,
    Run,
    Wall,
    Glide
}
