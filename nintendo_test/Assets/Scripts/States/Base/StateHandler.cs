﻿using System;
using UnityEngine;

public class StateHandler : MonoBehaviour
{
    public EState CurrentState { get; private set; }

    public event Action<EState> StateAction = delegate { };

    /// <summary>
    /// Called from animator
    /// </summary>
    public void SetState(EState state)
    {
        CurrentState = state;
        StateAction.Invoke(CurrentState);
    }
}
