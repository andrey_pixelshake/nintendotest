﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(StateHandler), typeof(Rigidbody))]
public abstract class State : MonoBehaviour
{   
    public AnimatorStateInfo StateInfo { get; private set; }
    public abstract EState StateType { get; }

    private bool _isStarterd;

	protected StateHandler _stateHandler;
    protected Rigidbody _rigidbody;
	protected Animator _animator;

    protected virtual void Awake()
    {
        enabled = false;
        _rigidbody = GetComponent<Rigidbody>();
        _stateHandler = GetComponent<StateHandler>();
		_animator = GetComponent<Animator>();
    }

    public void Init(AnimatorStateInfo stateInfo)
    {
        StateInfo = stateInfo;
    }

    public virtual void Enter()
    {
        _isStarterd = true;
        _stateHandler.SetState(StateType);
    }

    private void FixedUpdate()
    {
        if(_isStarterd)
        {
            Tick();
        }
    }

    public virtual void Tick()
    {
    }

    public virtual void Exit()
    {
        _isStarterd = true;
    }
}
