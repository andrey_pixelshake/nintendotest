﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField]
    private Settings _settings;

    private Rigidbody _rigidbody;
    private Vector3 _direction;
	private Animator _animator;

    void Awake () {
        _rigidbody = GetComponent<Rigidbody>();
		_animator = GetComponent<Animator>();
    }
	
	void Update ()
    {
        int count = 0;

        Vector3 dir = Vector2.zero;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            dir += new Vector3(0, 0, 1);
            count++;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            dir += new Vector3(0, 0, -1);
            count++;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            dir += new Vector3(1, 0, 0);
            count++;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            dir += new Vector3(-1, 0, 0);
            count++;
        }

        if (count > 0)
        {
            dir /= (float)count;

            _direction = Vector3.Lerp(_direction, dir, _settings.playerRotationSpeed * Time.deltaTime);

            _rigidbody.AddForce(_direction * _settings.playerSpeed);

            if (_rigidbody.velocity.magnitude > _settings.playerSpeedMax)
            {
                _rigidbody.velocity = _rigidbody.velocity.normalized * _settings.playerSpeedMax;
            }

            transform.rotation = Quaternion.LookRotation(_direction);

			_animator.SetBool("run", true);
        }
		else
		{
			_animator.SetBool("run", false);
		}
    }
}
