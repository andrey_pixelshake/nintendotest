﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class Settings : ScriptableObject
{
    [Header("Player")]
    public float playerSpeed = 1;
    public float playerSpeedMax = 1;
    public float playerRotationSpeed = 10;

    [Header("Npc")]
    public float npcRotationSpeed = 10;
	public float npcChangeDirectionTimeMin = 3;
	public float npcChangeDirectionTimeMax = 5;

    [Header("Penguins")]
    public float penguinIdleSpeed = 1;
    public float penguinRunSpeed = 1;
    public float penguinGlideSpeed = 1;
    public float penguinRotationSpeed = 10;
    public float penguinRunRotationSpeed = 3;
	public float penguinRunRotationMaxAngle = 60;
    public float penguinTimeChangeDirMin = 1f;
    public float penguinTimeChangeDirMax = 5f;
	public float penguinTimeCanWaitMin = 3;
	public float penguinTimeCanWaitMax = 5;
	[Range(0,1)]
	public float penguinGlideChance = 0.2f;
	[Range(0, 1)]
    public float penguinWaitDuringRunChance = 0.2f;

    [MenuItem("Assets/Create/Settings")]
    public static void CreateAsset()
    {
        var asset = CreateInstance<Settings>();
        ProjectWindowUtil.CreateAsset(asset, "Settings.asset");
    }
}
